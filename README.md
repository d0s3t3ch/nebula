# nebula

nebula is some scripts nd things to build live linux image for the memes and w0w experiments.  **Please understand *...I don't know what Im doing***. VoidLinux is the base for its easy to use build system and its lightness. Build uses MUSL compiler.. dunno what this means but it sounds stronk.  **Pull requests welcome!!**


### some goals
  * easy, cute, safe browsing exp for newbs leik me
  * include meme tools like gimp and inkscape
  * light weight for older systems
  * self building and updating (*with enough memory*)
  * tools for accepting tips and mining w0w
  * *have fun and learn more about linux!*


### system modifications
  * auto-login to xfce4
  * restrict firewall for safu browsing
  * uses steveblack/hosts file
  * add pyllyukko/user.js to firefox
  * new mac address on network up
  * validate ntp time against https

### included software

  * [xfce4](https://xfce.org/)
  * [gimp](https://www.gimp.org/)
  * [firefox](https://www.mozilla.org/en-US/firefox/new/)
  * [dino-im](https://dino.im/)
  * [vlc](https://www.videolan.org/vlc/)
  * [gnome-disks](https://wiki.gnome.org/Apps/Disks)
  * [cryptsetup-LUKS](https://gitlab.com/cryptsetup/cryptsetup)
  * [seahorse](https://wiki.gnome.org/Apps/Seahorse)
  * [keepassxc](https://keepassxc.org/)
  * [openntpd](http://www.openntpd.org/)
  * [monero-cli](https://github.com/monero-project/monero)
  * [monero-gui](https://github.com/monero-project/monero-gui)
  * [electrum](https://electrum.org/)
  * *add more live via xbps package system!!*

### it'd be nice todo..
  * dockerize image build
  * native wowd and wowlet packages
  * native xmrig package 
  * wizard for networks and persistent configs
  * custom grub splash


