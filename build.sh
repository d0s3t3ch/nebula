##set build hash id
buildHash=$(git rev-parse --short HEAD)

##update submodules
git submodule update --init --recursive --remote

##add locked_user.js from pyllyukko/user.js
cd external/user.js
make locked_user.js
cp locked_user.js ../../include/usr/lib/firefox/mozilla.cfg
cd ../..

##add hosts from stevenblack/hosts
cp external/hosts/hosts include/etc/hosts

##build nebula with sudo
cd external/void-mklive
make
sudo ./mklive.sh \
-a x86_64-musl \
-I ../../include \
-o ../../nebula-x86_64-$buildHash.iso \
-p "grub-i386-efi grub-x86_64-efi dialog cryptsetup lvm2 \
mdadm lxdm xinit xorg xfce4 gnome-themes-standard gnome-keyring \
network-manager-applet gvfs-afc gvfs-mtp gvfs-smb udisks2 \
firefox-esr tor seahorse pwgen keepassxc openntpd monero \
monero-gui electrum macchanger gnome-disk-utility runit-iptables \
gnupg2 dino dillo curl vlc"
